## Listar todos os utilizadores
**Método http:** `GET`

**URL:** `http://localhost:8080/api/v1/utilizadores`

**Exemplo via curl:** 
```
curl --header "Content-Type: application/json" --request GET http://localhost:8080/api/v1/utilizadores
```

## Consultar um utilizador
**Método http:** `GET`

**URL:** `http://localhost:8080/api/v1/utilizador/{identificador}`

**Exemplo via curl:** 
```
curl --header "Content-Type: application/json" --request GET http://localhost:8080/api/v1/utilizador/1
```

## Adicionar novo utilizador
**Método http:** `POST`

**URL:** `http://localhost:8080/api/v1/utilizador`

**JSON:** 
```json 
{ 
  "email": "joao@email.com", 
  "nome": "João da Silva", 
  "dataNascimento": "2000-04-01"
} 
```
**Exemplo via curl:** 
```
curl --header "Content-Type: application/json" --request POST --data '{ "email": "joao@email.com", "nome": "João da Silva", "dataNascimento": "2000-04-01"}' http://localhost:8080/api/v1/utilizador
```

## Atualizar utilizador
**Método http:** `PUT`

**URL:** `http://localhost:8080/api/v1/utilizador/{identificador}`

**JSON:**  
```json
{ 
  "email": "joao.silva@email.com",
  "nome": "João da Silva", 
  "dataNascimento": "2000-04-01"
}
```
**Exemplo via curl:** 
```
curl --header "Content-Type: application/json" --request PUT --data '{ "email": "joao.silva@email.com", "nome": "João da Silva", "dataNascimento": "2000-04-01"}' http://localhost:8080/api/v1/utilizador/3
```

## Excluir utilizador
**Método http:** `DELETE`

**URL:** `http://localhost:8080/api/v1/utilizador/{identificador}`

**Exemplo via curl:** 
```
curl --header "Content-Type: application/json" --request DELETE http://localhost:8080/api/v1/utilizador/3
```

## Docker-Compose
**Build**
 ```
- echo "Compiling the code..."
- echo "Compile complete."
 ```

 **Test**
 ```
- echo "Running unit tests..."
 ```

**Deploy**
```
- echo "Deploying application..."
- echo "Application successfully deployed."
```

## Pipeline CI/CD
**Build**
```
- echo "Compiling the code..."
- echo "Compile complete."
```

**Unit-Test**
```
- echo "Running unit tests... This will take about 60 seconds."
- sleep 60
- echo "Code coverage is 90%"
```

**Lint-Test**
```
- echo "Linting code... This will take about 10 seconds."
- sleep 10
- echo "No lint issues found."
```

**Deploy**
```
- echo "Deploying application..."
- echo "Application successfully deployed."
```
